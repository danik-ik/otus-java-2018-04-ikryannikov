package ru.otus.danik_ik.homework15.app.messages;

import ru.otus.danik_ik.homework15.app.DBAddressee;
import ru.otus.danik_ik.homework15.app.DBService;
import ru.otus.danik_ik.homework15.app.MsgToDB;
import ru.otus.danik_ik.homework15.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework15.messageSystem.Address;

public class MsgAddUser extends MsgToDB {
    private final UserDataSet userDataSet;

    public MsgAddUser(Address from, Address to, long requestId, UserDataSet userDataSet) {
        super(from, to, requestId);
        this.userDataSet = userDataSet;
    }

    @Override
    public void exec(DBAddressee dbAddressee) {
        try {
            UserDataSet userAnswer = dbAddressee.addUser(userDataSet);
            dbAddressee.getMS().sendMessage(new MsgUserToFrontend(getTo(), getFrom(), getId(), userAnswer));
        } catch (Exception e) {
            e.printStackTrace();
            dbAddressee.getMS().sendMessage(new MsgError(getTo(), getFrom(), getId(), "Ошибка при добавлении пользователя"));
        }
    }
}
