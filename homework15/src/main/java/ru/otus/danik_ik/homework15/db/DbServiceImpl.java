package ru.otus.danik_ik.homework15.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.otus.danik_ik.homework15.app.DBAddressee;
import ru.otus.danik_ik.homework15.app.MessageSystemContext;
import ru.otus.danik_ik.homework15.db.cachedstorage.DbServiceCached;
import ru.otus.danik_ik.homework15.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework15.messageSystem.Address;
import ru.otus.danik_ik.homework15.messageSystem.MessageSystem;

public class DbServiceImpl implements DBAddressee {
    private final Address address;
    private final MessageSystemContext context;
    private final DbServiceCached dbService;

    public DbServiceImpl(String address, MessageSystemContext context, DbServiceCached dbService) {
        this.address = new Address(address);
        this.context = context;
        this.dbService = dbService;
    }

    public void init() {
        context.getMessageSystem().addAddressee(this);
        context.setDbAddress(this.address);
    }

    @Override
    public UserDataSet getUserById(long id) {
        return dbService.read(id);
    }

    @Override
    public UserDataSet addUser(UserDataSet user) {
        dbService.save(user);
        return user;
    }

    @Override
    public UserDataSet update(UserDataSet user) {
        dbService.save(user);
        return user;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public MessageSystem getMS() {
        return context.getMessageSystem();
    }

}
