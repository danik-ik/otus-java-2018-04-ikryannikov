package ru.otus.danik_ik.homework15.app;

import ru.otus.danik_ik.homework15.db.storage.dataSets.UserDataSet;

import java.util.concurrent.Future;
import java.util.function.Consumer;

public interface FrontendToDBGate {
    void getUserById(long id, Consumer<UserDataSet> onComplete, Consumer<String> onError);
    void addUser(UserDataSet user, Consumer<UserDataSet> onComplete, Consumer<String> onError);
    void update(UserDataSet user, Consumer<UserDataSet> onComplete, Consumer<String> onError);

}
