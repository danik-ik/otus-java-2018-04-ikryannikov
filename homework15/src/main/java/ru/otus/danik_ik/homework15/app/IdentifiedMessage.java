package ru.otus.danik_ik.homework15.app;

import ru.otus.danik_ik.homework15.messageSystem.Address;
import ru.otus.danik_ik.homework15.messageSystem.Message;

public abstract class IdentifiedMessage extends Message {
    private final long id;
    public IdentifiedMessage(Address from, Address to, long id) {
        super(from, to);
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
