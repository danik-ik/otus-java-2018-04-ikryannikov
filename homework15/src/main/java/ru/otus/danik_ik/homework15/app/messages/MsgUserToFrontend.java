package ru.otus.danik_ik.homework15.app.messages;

import ru.otus.danik_ik.homework15.app.FrontendAddressee;
import ru.otus.danik_ik.homework15.app.FrontendService;
import ru.otus.danik_ik.homework15.app.MsgToFrontend;
import ru.otus.danik_ik.homework15.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework15.messageSystem.Address;

public class MsgUserToFrontend extends MsgToFrontend {
    private final UserDataSet userDataSet;

    public MsgUserToFrontend(Address from, Address to, long requestId, UserDataSet userDataSet) {
        super(from, to, requestId);
        this.userDataSet = userDataSet;
    }

    @Override
    public void exec(FrontendAddressee frontendAddressee) {
        frontendAddressee.completeRequest(getId(), userDataSet);
    }
}
