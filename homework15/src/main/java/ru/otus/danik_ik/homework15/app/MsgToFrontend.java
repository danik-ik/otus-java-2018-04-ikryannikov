package ru.otus.danik_ik.homework15.app;

import ru.otus.danik_ik.homework15.messageSystem.Address;
import ru.otus.danik_ik.homework15.messageSystem.Addressee;

/**
 * Created by tully.
 */
public abstract class MsgToFrontend extends IdentifiedMessage {
    public MsgToFrontend(Address from, Address to, long id) {
        super(from, to, id);
    }

    @Override
    public void exec(Addressee addressee) {
        if (addressee instanceof FrontendAddressee) {
            exec((FrontendAddressee) addressee);
        } else {
            //todo error!
        }
    }

    public abstract void exec(FrontendAddressee frontendAddressee);
}