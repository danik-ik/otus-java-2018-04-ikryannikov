package ru.otus.danik_ik.homework15.app;

import ru.otus.danik_ik.homework15.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework15.messageSystem.Addressee;

/**
 * Created by tully.
 */
public interface DBAddressee extends Addressee {
    void init();

    UserDataSet getUserById(long id);
    UserDataSet addUser(UserDataSet user);
    UserDataSet update(UserDataSet user);
}
