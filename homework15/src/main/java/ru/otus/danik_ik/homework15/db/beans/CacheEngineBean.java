package ru.otus.danik_ik.homework15.db.beans;

import ru.otus.danik_ik.homework15.db.cache.CacheEngineImpl;
import ru.otus.danik_ik.homework15.db.cache.CacheHelper;
import ru.otus.danik_ik.homework15.db.storage.dataSets.UserDataSet;

public class CacheEngineBean extends CacheEngineImpl<Long, UserDataSet> {
    public CacheEngineBean(int maxElements, long lifeTimeMs,
                           long idleTimeMs, boolean isEternal) {
        super(maxElements, lifeTimeMs, idleTimeMs,
                isEternal, CacheHelper.SoftEntryFactory());
    }
}

