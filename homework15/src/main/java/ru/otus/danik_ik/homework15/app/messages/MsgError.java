package ru.otus.danik_ik.homework15.app.messages;

import ru.otus.danik_ik.homework15.app.FrontendAddressee;
import ru.otus.danik_ik.homework15.app.MsgToFrontend;
import ru.otus.danik_ik.homework15.messageSystem.Address;

public class MsgError extends MsgToFrontend {
    private final String errorMessage;

    public MsgError(Address from, Address to, long id, String errorMessage) {
        super(from, to, id);
        this.errorMessage = errorMessage;
    }

    @Override
    public void exec(FrontendAddressee frontendAddressee) {
        frontendAddressee.handleException(this.getId(), errorMessage);
    }
}
