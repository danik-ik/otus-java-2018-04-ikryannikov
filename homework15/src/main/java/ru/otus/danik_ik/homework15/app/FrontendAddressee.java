package ru.otus.danik_ik.homework15.app;

import ru.otus.danik_ik.homework15.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework15.messageSystem.Addressee;

/**
 * Created by tully.
 */
public interface FrontendAddressee extends Addressee {
    void init();

    void completeRequest(long id, String response);

    void completeRequest(long id, UserDataSet dataSet);

    void handleException(long id, String exceptionInfo);
}

