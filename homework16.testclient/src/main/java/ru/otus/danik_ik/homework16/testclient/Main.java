package ru.otus.danik_ik.homework16.testclient;

import static java.util.concurrent.Executors.newFixedThreadPool;

public class Main
{
    private Frontend frontend;

    public static void main(String[] args ) throws Exception {
        new Main().start();
    }


    private void start() throws Exception {
        try (Frontend frontend = new Frontend()) {
            this.frontend = frontend;
            frontend.init();
            frontend.userInputLoop();
        }
    }
}
