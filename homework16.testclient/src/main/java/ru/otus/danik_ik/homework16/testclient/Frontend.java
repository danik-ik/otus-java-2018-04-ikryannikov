package ru.otus.danik_ik.homework16.testclient;

import ru.otus.danik_ik.homework16.messageserver.app.addressees.FrontendAddressee;
import ru.otus.danik_ik.homework16.messageserver.app.messages.EchoMessage;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgGetUserById;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgRegisterAddressee;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.app.serialization.GsonHelper;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MessageSystem;
import ru.otus.danik_ik.homework16.messageserver.server.channel.IncomingMessagesProcessor;
import ru.otus.danik_ik.homework16.messageserver.server.channel.SocketMsgWorker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.ParseException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Frontend implements FrontendAddressee, AutoCloseable {
    private static final Logger logger = Logger.getLogger(Frontend.class.getName());
    private Address ADDRESS = new Address(UUID.randomUUID().toString());
    private SocketMsgWorker worker;
    private Socket socket;
    private IncomingMessagesProcessor processor;

    @Override
    public void init() throws IOException {
        socket = new Socket("localhost", 5050);
        worker = new SocketMsgWorker(socket);
        worker.init();
        registerAddressee();
        startIncomingMessagesProcessor();
    }

    private void startIncomingMessagesProcessor() {
        IncomingMessagesProcessor processor = new IncomingMessagesProcessor(this, worker);
        this.processor = processor;
        processor.start();
    }

    private void registerAddressee() {
        Message msg = new MsgRegisterAddressee(ADDRESS);
        worker.send(msg);
    }

    public void userInputLoop() throws IOException, InterruptedException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line = in.readLine();
        while (!"".equals(line)) {
            try {
                executeCommand(line);
            } catch (ParseException e) {
                System.out.println(e.getMessage());
                System.out.println(line);
                System.out.println(generateMarker(e.getErrorOffset()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            line = in.readLine();
        }
    }

    private String generateMarker(int offset) {
        return IntStream.range(0, offset).mapToObj(i -> " ").collect(Collectors.joining(""))
                + "^";
    }

    private void executeCommand(String commandLine) throws ParseException {
        Matcher matcher = Pattern.compile("\\w+").matcher(commandLine);
        int nextPos = 0;
        if (matcher.find(nextPos)) {
            String cmd = matcher.group(0).toUpperCase();
            nextPos = matcher.end(0);
            System.out.println("> " + cmd.toUpperCase());
            switch (cmd) {
                case "GET":
                    if (!matcher.find(nextPos))
                        throw new ParseException("usage: get <int userId>", nextPos);
                    nextPos = matcher.start(0);
                    int userId = 0;
                    try {
                        userId = Integer.valueOf(matcher.group(0));
                    } catch (NumberFormatException e) {
                        throw new ParseException("usage: get <int userId>", nextPos);
                    }
                    Message msg = new MsgGetUserById(getAddress(), new Address("DB"), userId);
                    getMS().send(msg);
                    break;
                default:
                    sendEchoMessage(commandLine);
            }
        } else {
            sendEchoMessage(commandLine);

        }
    }

    private boolean isInteger(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void sendEchoMessage(String msg) {
        EchoMessage echoMessage = new EchoMessage(ADDRESS, msg);
        worker.send(echoMessage);
    }

    @Override
    public void completeRequest(UUID id, String response) {
        System.out.println(id + "> " + response);
    }

    @Override
    public void completeRequest(UUID id, UserDataSet userDataSet) {
        System.out.println(id + "> " + GsonHelper.toJson(userDataSet));
    }

    @Override
    public void handleException(UUID id, String exceptionInfo) {
        logger.log(Level.WARNING, exceptionInfo);
    }

    @Override
    public void handleInfo(UUID id, String info) {
        logger.log(Level.INFO, info);
    }

    @Override
    public Address getAddress() {
        return ADDRESS;
    }

    @Override
    public MessageSystem getMS() {
        return worker;
    }

    @Override
    public void close() throws Exception {
        socket.close();
        worker.close();
        processor.close();
    }
}
