package ru.otus.danik_ik.homework16.messageserver.app.addressees;

import ru.otus.danik_ik.homework16.messageserver.app.CommonMessagesHandler;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;

/**
 * Created by tully.
 */
public interface DBAddressee extends Addressee, CommonMessagesHandler {
    void init();

    UserDataSet getUserById(long id);
    UserDataSet addUser(UserDataSet user);
    UserDataSet update(UserDataSet user);
}
