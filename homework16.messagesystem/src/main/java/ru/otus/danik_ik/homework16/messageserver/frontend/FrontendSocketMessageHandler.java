package ru.otus.danik_ik.homework16.messageserver.frontend;

import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;

import java.util.UUID;
import java.util.function.BiConsumer;

public interface FrontendSocketMessageHandler {
    BiConsumer<UUID, UserDataSet> getResponseHandler();

    BiConsumer<UUID, String> getExceptionHandler();

    BiConsumer<UUID, String> getInfoHandler();
}
