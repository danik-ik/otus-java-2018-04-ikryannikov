package ru.otus.danik_ik.homework16.messageserver.app;

import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgException;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MsgKind;

import java.util.UUID;

public abstract class ExchangeMessage extends Message {
    protected ExchangeMessage(Class<? extends Message> klass) {
        super(klass);
    }

    public ExchangeMessage(Address from, Address to) {
        super(from, to);
    }

    public ExchangeMessage(Address from, Address to, UUID requestId) {
        super(from, to, requestId);
    }

    @Override
    public MsgKind getKind() {
        return MsgKind.EXCHANGE;
    }

    public abstract void exec(Addressee addressee);

    public void returnInvalidAddresseeResponse(Addressee addressee, Class mustBe) {
        addressee.getMS().send(new MsgException(addressee.getAddress(), this,
                String.format("Invalid class of addressee for %s: must be %s, but was %s",
                        this.getClass().getCanonicalName(),
                        CommonMessagesHandler.class.getCanonicalName(),
                        addressee.getClass().getCanonicalName())));

    }

}
