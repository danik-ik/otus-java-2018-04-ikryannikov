package ru.otus.danik_ik.homework16.messageserver.app;

import ru.otus.danik_ik.homework16.messageserver.app.addressees.ControlAddressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MsgKind;

public abstract class ControlMessage extends Message {
    protected ControlMessage(Class<? extends Message> klass) {
        super(klass);
    }

    public ControlMessage(Address from, Address to) {
        super(from, to);
    }

    @Override
    public MsgKind getKind() {
        return MsgKind.CONTROL;
    }

    public abstract void execControlMessage(ControlAddressee control, MsgWorker client);
}
