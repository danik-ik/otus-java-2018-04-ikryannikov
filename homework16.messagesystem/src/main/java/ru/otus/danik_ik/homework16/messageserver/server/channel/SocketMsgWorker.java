package ru.otus.danik_ik.homework16.messageserver.server.channel;

import org.json.simple.parser.ParseException;
import ru.otus.danik_ik.homework16.messageserver.app.annotations.Blocks;
import ru.otus.danik_ik.homework16.messageserver.app.messages.PoisonMessage;
import ru.otus.danik_ik.homework16.messageserver.app.serialization.GsonHelper;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by tully.
 */
public class SocketMsgWorker implements MsgWorker {
    private static final Logger logger = Logger.getLogger(SocketMsgWorker.class.getName());
    {
        logger.setLevel(Level.ALL);
        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.ALL);
        logger.addHandler(consoleHandler);
    }
    private static final int WORKERS_COUNT = 2;

    private final BlockingQueue<Message> output = new LinkedBlockingQueue<>();
    private final BlockingQueue<Message> input = new LinkedBlockingQueue<>();

    private final ExecutorService executor;
    private final Socket socket;

    private Consumer<MsgWorker> onClose;

    public SocketMsgWorker(Socket socket) {
        this.socket = socket;
        this.executor = Executors.newFixedThreadPool(WORKERS_COUNT);
    }

    @Override
    public void send(Message message) {
        output.add(message);
    }

    @Override
    public Message pool() {
        return input.poll();
    }

    @Override
    public Message take() throws InterruptedException {
        return input.take();
    }

    @Override
    public void close() {
        executor.shutdown();
        output.add(new PoisonMessage()); // "Poison" message to break take()
        input.add(new PoisonMessage());
        if (onClose != null) onClose.accept(this);
    }

    public void init() {
        executor.execute(this::sendMessageLoop);
        executor.execute(this::receiveMessageLoop);
    }

    @Blocks
    private void sendMessageLoop() {
        try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {
            while (socket.isConnected()) {
                try {
                    Message message = output.take(); //blocks
                    if (message instanceof PoisonMessage) return;
                    String json = GsonHelper.toJson(message);
                    logger.log(Level.FINEST, "Msg to send: " + json);
                    out.println(json);
                    out.println();//line with json + an empty line
                    logger.log(Level.FINEST, "Sent");
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage());
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    @Blocks
    private void receiveMessageLoop() {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            String inputLine;
            StringBuilder stringBuilder = new StringBuilder();
            while ((inputLine = in.readLine()) != null) { //blocks
                //System.out.println("Message received: " + inputLine);
                stringBuilder.append(inputLine);
                try {
                    if (inputLine.isEmpty()) { //empty line is the end of the message
                        String json = stringBuilder.toString();
                        logger.log(Level.FINEST, "Msg received: " + json);
                        Message message = null;
                        try {
                            message = GsonHelper.getMsgFromJSON(json);
                            input.add(message);
                        } catch (ClassNotFoundException | ParseException e) {
                            logger.log(Level.SEVERE, e.getMessage());
                        }
                        stringBuilder = new StringBuilder();
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage());
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage());
        } finally {
            close();
        }
    }

    public Consumer<MsgWorker> getOnClose() {
        return onClose;
    }

    public void setOnClose(Consumer<MsgWorker> onClose) {
        this.onClose = onClose;
    }
}
