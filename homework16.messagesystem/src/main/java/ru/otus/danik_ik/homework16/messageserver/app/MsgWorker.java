package ru.otus.danik_ik.homework16.messageserver.app;


import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MessageSystem;
import ru.otus.danik_ik.homework16.messageserver.app.annotations.Blocks;

import java.io.Closeable;

/**
 * Created by tully.
 */
public interface MsgWorker extends Closeable, MessageSystem {
    void send(Message message);

    Message pool();

    @Blocks
    Message take() throws InterruptedException;

    void close();
}
