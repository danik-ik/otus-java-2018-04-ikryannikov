package ru.otus.danik_ik.homework16.messageserver.app.messages;

import ru.otus.danik_ik.homework16.messageserver.app.CommonMessagesHandler;
import ru.otus.danik_ik.homework16.messageserver.app.MsgCommon;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;

public class MsgException extends MsgCommon {
    private String msg;

    public MsgException(Address from, Message request, String msg) {
        super(from, request.getFrom(), request.getRequestId());
        this.msg = msg;
    }

    @Override
    public void exec(CommonMessagesHandler addressee) {
        addressee.handleException(this.getRequestId(), msg);
    }
}
