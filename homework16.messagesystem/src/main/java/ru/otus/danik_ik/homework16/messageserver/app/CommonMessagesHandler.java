package ru.otus.danik_ik.homework16.messageserver.app;

import java.util.UUID;

public interface CommonMessagesHandler {
    void handleException(UUID id, String exceptionInfo);

    void handleInfo(UUID id, String info);
}
