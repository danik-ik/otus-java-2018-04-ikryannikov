package ru.otus.danik_ik.homework16.messageserver.server.channel;

import ru.otus.danik_ik.homework16.messageserver.app.ExchangeMessage;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;
import ru.otus.danik_ik.homework16.messageserver.app.annotations.Blocks;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgException;
import ru.otus.danik_ik.homework16.messageserver.app.messages.PoisonMessage;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.server.SocketMsgServer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IncomingMessagesProcessor implements AutoCloseable {
    private static final Logger logger = Logger.getLogger(SocketMsgServer.class.getName());

    private final Addressee addressee;
    private final MsgWorker worker;
    private final ExecutorService executor;

    private volatile boolean isTerminated;

    public IncomingMessagesProcessor(Addressee addressee, MsgWorker worker, ExecutorService executor) {
        this.addressee = addressee;
        this.worker = worker;
        this.executor = executor;
    }

    public IncomingMessagesProcessor(Addressee addressee, MsgWorker worker) {
        this(addressee, worker, Executors.newSingleThreadExecutor());
    }

    @Blocks
    public void start() {
        executor.execute(this::incomingLoop);
    }

    private void incomingLoop() {
        ExchangeMessage exchangeMessage = getExchangeMessage(worker);
        if (exchangeMessage == null) return;
        try {
            exchangeMessage.exec(addressee);
        } catch (Exception e) {
            worker.send(new MsgException(addressee.getAddress(), exchangeMessage, e.getMessage()));
        }
        if (!isTerminated)
            executor.execute(this::incomingLoop);
    }

    @Blocks
    public static ExchangeMessage getExchangeMessage(MsgWorker worker) {
        try {
            Message takenMessage = worker.take();
            if (takenMessage instanceof PoisonMessage) return null;
            return (ExchangeMessage) takenMessage;
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage());
            return null;
        }
    }

    @Override
    public void close() throws Exception {
        isTerminated = true;
        executor.shutdown();
    }

    public boolean isTerminated() {
        return isTerminated;
    }
}
