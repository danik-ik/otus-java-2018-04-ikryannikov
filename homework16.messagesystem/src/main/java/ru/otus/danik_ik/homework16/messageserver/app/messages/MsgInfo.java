package ru.otus.danik_ik.homework16.messageserver.app.messages;

import ru.otus.danik_ik.homework16.messageserver.app.CommonMessagesHandler;
import ru.otus.danik_ik.homework16.messageserver.app.MsgCommon;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;

import java.util.UUID;

public class MsgInfo extends MsgCommon {
    private String msg;

    public MsgInfo(Address from, Address to, String msg) {
        super(from, to);
        this.msg = msg;
    }

    public MsgInfo(Address from, Address to, UUID requestId, String msg) {
        super(from, to, requestId);
        this.msg = msg;
    }

    @Override
    public void exec(CommonMessagesHandler addressee) {
        addressee.handleInfo(this.getRequestId(), msg);
    }
}
