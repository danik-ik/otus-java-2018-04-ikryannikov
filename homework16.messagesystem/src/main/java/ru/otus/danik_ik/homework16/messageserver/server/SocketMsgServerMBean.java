package ru.otus.danik_ik.homework16.messageserver.server;

/**
 * Created by tully.
 */
public interface SocketMsgServerMBean {
    boolean getRunning();

    void setRunning(boolean running);
}
