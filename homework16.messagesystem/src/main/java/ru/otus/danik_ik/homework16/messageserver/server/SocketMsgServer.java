package ru.otus.danik_ik.homework16.messageserver.server;

import ru.otus.danik_ik.homework16.messageserver.app.ControlMessage;
import ru.otus.danik_ik.homework16.messageserver.app.Exceptions.AddressAlreadyRegisteredError;
import ru.otus.danik_ik.homework16.messageserver.app.ExchangeMessage;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;
import ru.otus.danik_ik.homework16.messageserver.app.addressees.ControlAddressee;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgException;
import ru.otus.danik_ik.homework16.messageserver.app.messages.PoisonMessage;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MessageSystem;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MsgKind;
import ru.otus.danik_ik.homework16.messageserver.app.annotations.Blocks;
import ru.otus.danik_ik.homework16.messageserver.server.channel.SocketMsgWorker;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by tully.
 */
public class SocketMsgServer implements SocketMsgServerMBean, MessageSystem {
    private static final Logger logger = Logger.getLogger(SocketMsgServer.class.getName());

    private static final int THREADS_NUMBER = 1;
    private static final int PORT = 5050;

    private final ExecutorService executor;
    private final List<MsgWorker> workers;
    private final ControlAddressee controlAddressee = new MsgServerControl();

    private final Map<MsgKind, BiConsumer<MsgWorker, Message>> msgHandlersByKind;
    {
        Map<MsgKind, BiConsumer<MsgWorker, Message>> handlers = new HashMap<>();
        handlers.put(MsgKind.CONTROL, this::handleControlMessage);
        handlers.put(MsgKind.EXCHANGE, this::handleExchangeMessage);

        msgHandlersByKind = Collections.unmodifiableMap(handlers);
    }
    private final Map<Address, MsgWorker> workersByAddress = new ConcurrentHashMap<>();
    private final Map<MsgWorker, Address> addressByWorker = new ConcurrentHashMap<>();

    private void handleExchangeMessage(MsgWorker client, Message message) {
        this.send(message);
    }

    private void handleControlMessage(MsgWorker client, Message message) {
        ((ControlMessage)message).execControlMessage(this.controlAddressee, client);
    }

    private volatile boolean isRunning = true;
    private ServerSocket serverSocket;

    public SocketMsgServer() {
        executor = Executors.newFixedThreadPool(THREADS_NUMBER);
        workers = new CopyOnWriteArrayList<>();
    }

    @Blocks
    public void start() throws Exception {
        try {
            controlAddressee.registerAddressee(controlAddressee.getAddress(), controlAddressee);
        } catch (AddressAlreadyRegisteredError e) {
            logger.log(Level.WARNING, e.getMessage());
        }

        executor.submit(this::receiving);

        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            this.serverSocket = serverSocket;
            logger.info("Server started on port: " + serverSocket.getLocalPort());
            while (!executor.isShutdown()) {
                Socket socket = null; //blocks
                try {
                    socket = serverSocket.accept();
                } catch (SocketException e) {
                    return;
                }
                SocketMsgWorker client = new SocketMsgWorker(socket);
                client.init();
                client.setOnClose(it ->{
                    workers.remove(it);
                    Address address = addressByWorker.get(it);
                    if (address != null) {
                        addressByWorker.remove(it);
                        workersByAddress.remove(address);
                        logger.log(Level.INFO, String.format("Connection closed for %s", address.getId()));
                    } else {
                        logger.log(Level.INFO, "Connection closed for unknown addressee");
                    }
                });
                workers.add(client);
            }
        }
    }

    @SuppressWarnings("InfiniteLoopStatement")
    private void receiving() {
        while (isRunning) {
            for (MsgWorker client : workers) {
                Message message = client.pool();
                while (message != null) {
                    dispatchMessage(client, message);
                    message = client.pool();
                }
            }
        }
    }

    private void dispatchMessage(MsgWorker client, Message message) {
        msgHandlersByKind.get(message.getKind()).accept(client, message);
    }

    @Override
    public boolean getRunning() {
        return isRunning;
    }

    @Override
    public void setRunning(boolean running) {
        isRunning = running;
        if (!running) {
            executor.shutdown();
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger.info("Bye.");
        }
    }

    @Override
    public void send(Message message) {
        MsgWorker worker = workersByAddress.get(message.getTo());
        if (worker != null) {
            worker.send(message);
        } else {
            newExceptionMessage(message, String.format("Registered addressee not found: %s", message.getTo().getId()));
        }
    }

    protected void newExceptionMessage(Message request, String messageInfo) {
        logger.log(Level.WARNING, messageInfo);
        send(new MsgException(this.controlAddressee.getAddress(), request, messageInfo));
    }

    private class MsgServerControl implements ControlAddressee {

        @Override
        public void registerAddressee(Address address, MsgWorker worker) throws AddressAlreadyRegisteredError {
            if (workersByAddress.containsKey(address)) {
                String messageInfo = String.format("Address already registered: %s", address.getId());
                logger.log(Level.WARNING, messageInfo);
                throw new AddressAlreadyRegisteredError(messageInfo);
            }
            workersByAddress.put(address, worker);
            addressByWorker.put(worker, address);
            logger.log(Level.INFO, "Registered addressee: %s", address.getId());
        }

        @Override
        public Address getAddress() {
            return new Address(MsgKind.CONTROL.toString());
        }

        @Override
        public MessageSystem getMS() {
            return SocketMsgServer.this;
        }

        @Override
        public void handleException(UUID id, String exceptionInfo) {
            logger.log(Level.WARNING, exceptionInfo);
        }

        @Override
        public void handleInfo(UUID id, String info) {
            logger.log(Level.INFO, info);
        }

        @Override
        public void send(Message message) {
            if (message instanceof PoisonMessage) return;
            try {
                ((ExchangeMessage) message).exec(this);
            } catch (ClassCastException e) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }

        @Override
        public Message pool() {
            throw new UnsupportedOperationException(this.getClass().getCanonicalName() + ".poll()");
        }

        @Override
        public Message take() throws InterruptedException {
            throw new UnsupportedOperationException(this.getClass().getCanonicalName() + ".take()");
        }

        @Override
        public void close() {
            // do nothing
        }
    }
}
