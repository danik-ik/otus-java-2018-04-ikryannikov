package ru.otus.danik_ik.homework16.messageserver.app.addressees;

import ru.otus.danik_ik.homework16.messageserver.app.CommonMessagesHandler;
import ru.otus.danik_ik.homework16.messageserver.app.Exceptions.AddressAlreadyRegisteredError;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;

public interface ControlAddressee extends Addressee, CommonMessagesHandler, MsgWorker {
    void registerAddressee(Address address, MsgWorker worker) throws AddressAlreadyRegisteredError;
}
