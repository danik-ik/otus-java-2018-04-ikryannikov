package ru.otus.danik_ik.homework16.messageserver.app;

import ru.otus.danik_ik.homework16.messageserver.app.addressees.DBAddressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;

import java.util.UUID;

/**
 * Created by tully.
 */
public abstract class MsgToDB extends ExchangeMessage {
    public MsgToDB(Address from, Address to) {
        super(from, to);
    }

    public MsgToDB(Class<? extends Message> klass) {
        super(klass);
    }

    public MsgToDB(Address from, Address to, UUID requestId) {
        super(from, to, requestId);
    }

    @Override
    public void exec(Addressee addressee) {
        DBAddressee dbAddressee = null;
        try {
            dbAddressee = (DBAddressee) addressee;
        } catch (ClassCastException e) {
            returnInvalidAddresseeResponse(addressee, DBAddressee.class);
            return;
        }
        exec(dbAddressee);

    }

    public abstract void exec(DBAddressee dbAddressee);
}
