package ru.otus.danik_ik.homework16.messageserver.app.messages;

import ru.otus.danik_ik.homework16.messageserver.app.ControlMessage;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;
import ru.otus.danik_ik.homework16.messageserver.app.addressees.ControlAddressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;

/**
 * Сообщение предназначено для прерывания ожидания получения сообщения из очереди.
 */
public class PoisonMessage extends ControlMessage {

    protected PoisonMessage(Class<? extends Message> klass) {
        super(klass);
    }

    @Override
    public void execControlMessage(ControlAddressee control, MsgWorker client) {
        // do nothing
    }

    public PoisonMessage() {
        super(PoisonMessage.class);
    }

}
