package ru.otus.danik_ik.homework16.messageserver.app.Exceptions;

public class AddressAlreadyRegisteredError extends Throwable {
    public AddressAlreadyRegisteredError() {
    }

    public AddressAlreadyRegisteredError(String message) {
        super(message);
    }

    public AddressAlreadyRegisteredError(String message, Throwable cause) {
        super(message, cause);
    }

    public AddressAlreadyRegisteredError(Throwable cause) {
        super(cause);
    }
}
