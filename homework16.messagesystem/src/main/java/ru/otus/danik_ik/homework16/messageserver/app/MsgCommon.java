package ru.otus.danik_ik.homework16.messageserver.app;

import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgException;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;

import java.util.UUID;

/**
 * Created by tully.
 */
public abstract class MsgCommon extends ExchangeMessage {
    public MsgCommon(Address from, Address to) {
        super(from, to);
    }

    protected MsgCommon(Class<? extends Message> klass) {
        super(klass);
    }

    public MsgCommon(Address from, Address to, UUID requestId) {
        super(from, to, requestId);
    }

    @Override
    public void exec(Addressee addressee) {
        CommonMessagesHandler handler = null;
        try {
            handler = (CommonMessagesHandler) addressee;
        } catch (ClassCastException e) {
            returnInvalidAddresseeResponse(addressee, CommonMessagesHandler.class);
            return;
        }
        exec(handler);
    }

    public abstract void exec(CommonMessagesHandler addressee);
}