package ru.otus.danik_ik.homework16.messageserver.app.messages;

import ru.otus.danik_ik.homework16.messageserver.app.addressees.DBAddressee;
import ru.otus.danik_ik.homework16.messageserver.app.MsgToDB;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;

import java.util.UUID;

public class MsgGetUserById extends MsgToDB {
    private final long userId;

    public MsgGetUserById(Address from, Address to, UUID requestId, long userId) {
        super(from, to, requestId);
        this.userId = userId;
    }

    public MsgGetUserById(Address from, Address to, long userId) {
        super(from, to);
        this.userId = userId;
    }

    @Override
    public void exec(DBAddressee dbAddressee) {
        try {
            UserDataSet userAnswer = dbAddressee.getUserById(userId);
            if (userAnswer == null) {
                dbAddressee.getMS().send(new MsgException(getTo(), this, String.format("Пользователь не найден: %d", userId)));
                return;
            }
            dbAddressee.getMS().send(new MsgUserToFrontend(getTo(), getFrom(), getRequestId(), userAnswer));
        } catch (Exception e) {
            e.printStackTrace();
            dbAddressee.getMS().send(new MsgException(getTo(), this, "Ошибка при получении пользователя"));
        }
    }
}
