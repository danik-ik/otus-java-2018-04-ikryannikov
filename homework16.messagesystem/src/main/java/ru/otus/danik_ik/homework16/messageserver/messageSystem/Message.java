package ru.otus.danik_ik.homework16.messageserver.messageSystem;

import java.beans.Transient;
import java.lang.annotation.Inherited;
import java.util.UUID;

/**
 * Created by tully.
 */
public abstract class Message {
    public static final String CLASS_NAME_VARIABLE = "className";

    private final String className;

    private Address from;
    private Address to;
    private UUID requestId;

    protected Message(Class<? extends Message> klass) {
        this.className = klass.getName();
    }

    public Message(Address from, Address to) {
        this(from, to, UUID.randomUUID());
    }

    public Message(Address from, Address to, UUID requestId) {
        className = this.getClass().getName();
        this.from = from;
        this.to = to;
        this.requestId = requestId;
    }

    public Address getFrom() {
        return from;
    }

    public Address getTo() {
        return to;
    }

    @Transient
    public abstract MsgKind getKind();

    public UUID getRequestId() {
        return requestId;
    }
}
