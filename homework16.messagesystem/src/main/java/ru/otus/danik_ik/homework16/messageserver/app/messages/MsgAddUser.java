package ru.otus.danik_ik.homework16.messageserver.app.messages;

import ru.otus.danik_ik.homework16.messageserver.app.addressees.DBAddressee;
import ru.otus.danik_ik.homework16.messageserver.app.MsgToDB;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;

import java.util.UUID;

public class MsgAddUser extends MsgToDB {
    private final UserDataSet userDataSet;

    public MsgAddUser(Address from, Address to, UUID requestId, UserDataSet userDataSet) {
        super(from, to, requestId);
        this.userDataSet = userDataSet;
    }

    public MsgAddUser(Address from, Address to, UserDataSet userDataSet) {
        super(from, to);
        this.userDataSet = userDataSet;
    }

    @Override
    public void exec(DBAddressee dbAddressee) {
        try {
            UserDataSet userAnswer = dbAddressee.addUser(userDataSet);
            dbAddressee.getMS().send(new MsgUserToFrontend(getTo(), getFrom(), getRequestId(), userAnswer));
        } catch (Exception e) {
            e.printStackTrace();
            dbAddressee.getMS().send(new MsgException(getTo(), this, "Ошибка при добавлении пользователя"));
        }
    }
}
