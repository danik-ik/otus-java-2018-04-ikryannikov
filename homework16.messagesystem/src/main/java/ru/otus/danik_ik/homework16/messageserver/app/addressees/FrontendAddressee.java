package ru.otus.danik_ik.homework16.messageserver.app.addressees;

import ru.otus.danik_ik.homework16.messageserver.app.CommonMessagesHandler;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by tully.
 */
public interface FrontendAddressee extends Addressee, CommonMessagesHandler {
    void init() throws IOException;

    void completeRequest(UUID id, String response);

    void completeRequest(UUID id, UserDataSet userDataSet);
}

