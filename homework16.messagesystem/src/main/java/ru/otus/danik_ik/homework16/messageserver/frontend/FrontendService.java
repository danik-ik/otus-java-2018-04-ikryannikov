package ru.otus.danik_ik.homework16.messageserver.frontend;

import ru.otus.danik_ik.homework16.messageserver.app.addressees.FrontendAddressee;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;

import java.util.UUID;
import java.util.function.BiConsumer;

public interface FrontendService extends AutoCloseable, FrontendAddressee {
    void init();

    void getUserById(long id, FrontendSocketMessageHandler socket);

    void addUser(UserDataSet dataSet, FrontendSocketMessageHandler socket);

    void update(UserDataSet dataSet, FrontendSocketMessageHandler socket);
}
