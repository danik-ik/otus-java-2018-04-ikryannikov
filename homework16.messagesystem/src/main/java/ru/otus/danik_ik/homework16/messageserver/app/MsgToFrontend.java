package ru.otus.danik_ik.homework16.messageserver.app;

import ru.otus.danik_ik.homework16.messageserver.app.addressees.FrontendAddressee;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgException;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Addressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;

import java.util.UUID;

/**
 * Created by tully.
 */
public abstract class MsgToFrontend extends ExchangeMessage {
    public MsgToFrontend(Address from, Address to) {
        super(from, to);
    }

    protected MsgToFrontend(Class<? extends Message> klass) {
        super(klass);
    }

    public MsgToFrontend(Address from, Address to, UUID requestId) {
        super(from, to, requestId);
    }

    @Override
    public void exec(Addressee addressee) {
        FrontendAddressee frontend = null;
        try {
            frontend = (FrontendAddressee) addressee;
        } catch (ClassCastException e) {
            returnInvalidAddresseeResponse(addressee, FrontendAddressee.class);
            return;
        }
        exec(frontend);
    }

    public abstract void exec(FrontendAddressee frontendAddressee);
}