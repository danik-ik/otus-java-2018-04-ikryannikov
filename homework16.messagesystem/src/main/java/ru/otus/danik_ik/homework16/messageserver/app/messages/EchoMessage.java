package ru.otus.danik_ik.homework16.messageserver.app.messages;

import ru.otus.danik_ik.homework16.messageserver.app.MsgToFrontend;
import ru.otus.danik_ik.homework16.messageserver.app.addressees.FrontendAddressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;

public class EchoMessage extends MsgToFrontend {
    private String echoString;

    protected EchoMessage(Class<? extends Message> klass) {
        super(klass);
    }

    public EchoMessage(Address from, String echoString) {
        super(from, from);
        this.echoString = echoString;
    }

    @Override
    public void exec(FrontendAddressee frontendAddressee) {
        frontendAddressee.completeRequest(this.getRequestId(), this.echoString);
    }
}
