package ru.otus.danik_ik.homework16.messageserver.messageSystem;

public enum MsgKind {
    CONTROL,
    EXCHANGE,
}
