package ru.otus.danik_ik.homework16.messageserver.cache;

/**
 * Created by danik_ik.
 */
@SuppressWarnings("WeakerAccess")
public interface CacheEntry<K, V> {

    K getKey();

    V getValue();

    long getCreationTime();

    long getLastAccessTime();

    void setAccessed();
}
