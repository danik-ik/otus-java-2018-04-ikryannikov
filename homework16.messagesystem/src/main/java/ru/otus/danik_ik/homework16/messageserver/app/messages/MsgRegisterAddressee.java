package ru.otus.danik_ik.homework16.messageserver.app.messages;

import ru.otus.danik_ik.homework16.messageserver.app.ControlMessage;
import ru.otus.danik_ik.homework16.messageserver.app.Exceptions.AddressAlreadyRegisteredError;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;
import ru.otus.danik_ik.homework16.messageserver.app.addressees.ControlAddressee;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MsgKind;

public class MsgRegisterAddressee extends ControlMessage {
    public MsgRegisterAddressee(Address from) {
        super(from, new Address(MsgKind.CONTROL.toString()));
    }

    public MsgRegisterAddressee(Class<? extends Message> klass) {
        super(klass);
    }

    @Override
    public void execControlMessage(ControlAddressee control, MsgWorker client) {
        try {
            control.registerAddressee(this.getFrom(), client);
            control.getMS().send(new MsgInfo(getTo(), getFrom(), getRequestId(), String.format("Registered addressee: %s", getFrom().getId())));
        } catch (AddressAlreadyRegisteredError e) {
            control.getMS().send(new MsgException(control.getAddress(), this, e.getMessage()));
        }
    }
}
