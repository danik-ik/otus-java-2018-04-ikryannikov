package ru.otus.danik_ik.homework16.messageserver.app.messages;

import ru.otus.danik_ik.homework16.messageserver.app.MsgToFrontend;
import ru.otus.danik_ik.homework16.messageserver.app.addressees.FrontendAddressee;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;

import java.util.UUID;

public class MsgUserToFrontend extends MsgToFrontend {
    private final UserDataSet userDataSet;

    public MsgUserToFrontend(Address from, Address to, UUID requestId, UserDataSet userDataSet) {
        super(from, to, requestId);
        this.userDataSet = userDataSet;
    }

    @Override
    public void exec(FrontendAddressee frontendAddressee) {
        frontendAddressee.completeRequest(getRequestId(), userDataSet);
    }
}
