package ru.otus.danik_ik.homework16.messageserver.messageSystem;

/**
 * @author tully
 */
public interface MessageSystem {

    public void send(Message message);

}
