package ru.otus.danik_ik.homework16.messageserver.app.serialization;

import com.google.gson.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;

import java.lang.reflect.Type;

public class GsonHelper {
    public static String toJson(Object o) {
        String json = newGsonTo().toJson(o);
        return json;
    }

    public static UserDataSet userFromJson(String json) {
        UserDataSet user = newGsonFrom().fromJson(json, UserDataSet.class);
        return user;
    }

    public static Message getMsgFromJSON(String json) throws ParseException, ClassNotFoundException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
        String className = (String) jsonObject.get(Message.CLASS_NAME_VARIABLE);
        Class<?> msgClass = Class.forName(className);
        return (Message) newGsonFrom().fromJson(json, msgClass);
    }

    private static Gson newGsonTo() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(UserDataSet.class, new UserDataSetSerializer())
                .create();
    }

    private static Gson newGsonFrom() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(UserDataSet.class, new UserDataSetDeserializer())
                .create();
    }

    static class UserDataSetSerializer implements JsonSerializer<UserDataSet> {
        @Override
        public JsonElement serialize(UserDataSet src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject result = new JsonObject();

            result.addProperty("id", src.getID());
            result.addProperty("name", src.getName());
            result.addProperty("address", src.getStreet());

            return result;
        }
    }

    static class UserDataSetDeserializer implements JsonDeserializer<UserDataSet>
    {
        @Override
        public UserDataSet deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
        {
            JsonObject jo = json.getAsJsonObject();

            UserDataSet user = new UserDataSet();
            if (jo.has("id"))
                user.setID(jo.get("id").getAsLong());
            user.setName(jo.get("name").getAsString());
            user.setStreet(jo.get("address").getAsString());

            return user;
        }
    }

}
