package ru.otus.danik_ik.homework16.messageserver.app.serialization;

import com.google.gson.Gson;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgUserToFrontend;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;

import java.util.UUID;

import static org.junit.Assert.*;

public class GsonHelperTest {

    @Test
    public void msgUserToJson() {
        UserDataSet user = new UserDataSet();
        user.setName("mr. Trololo");
        user.setStreet("Nowhere");
        MsgUserToFrontend msg = new MsgUserToFrontend(new Address("DB"), new Address("frontend"), UUID.randomUUID(), user);
        String json = GsonHelper.toJson(msg);
        System.out.println(json);
        assertNotNull(json);
        assertTrue(json.contains("mr. Trololo"));
        assertTrue(json.contains("Nowhere"));
    }

    @Test
    public void getMsgFromJSON() throws ParseException, ClassNotFoundException {
        Message msg = GsonHelper.getMsgFromJSON("{\n" +
                "  \"userDataSet\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"mr. Trololo\",\n" +
                "    \"address\": \"Nowhere\"\n" +
                "  },\n" +
                "  \"className\": \"ru.otus.danik_ik.homework16.messageserver.app.messages.MsgUserToFrontend\",\n" +
                "  \"from\": {\n" +
                "    \"id\": \"DB\"\n" +
                "  },\n" +
                "  \"to\": {\n" +
                "    \"id\": \"frontend\"\n" +
                "  },\n" +
                "  \"requestId\": \"7d7c62a2-9db0-4d71-8c37-73557bcccf26\"\n" +
                "}\n");

    }
}