package ru.otus.danik_ik.homework16.dbserviceapp;

import ru.otus.danik_ik.homework16.dbserviceapp.cachedstorage.DbServiceCached;
import ru.otus.danik_ik.homework16.messageserver.app.MessageSystemContext;
import ru.otus.danik_ik.homework16.messageserver.app.addressees.DBAddressee;
import ru.otus.danik_ik.homework16.messageserver.db.storage.DBService;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MessageSystem;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbServiceImpl implements DBAddressee {
    private static final Logger logger = Logger.getLogger(DbServiceImpl.class.getName());

    private final Address address;
    private final MessageSystemContext context;
    private final DBService dbService;

    public DbServiceImpl(String address, MessageSystemContext context, DBService dbService) {
        this.address = new Address(address);
        this.context = context;
        this.dbService = dbService;
    }

    public void init() {
    }

    @Override
    public UserDataSet getUserById(long id) {
        return dbService.read(id);
    }

    @Override
    public UserDataSet addUser(UserDataSet user) {
        dbService.save(user);
        return user;
    }

    @Override
    public UserDataSet update(UserDataSet user) {
        dbService.save(user);
        return user;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public MessageSystem getMS() {
        return context.getMessageSystem();
    }

    @Override
    public void handleException(UUID id, String exceptionInfo) {
        logger.log(Level.WARNING, exceptionInfo);
    }

    @Override
    public void handleInfo(UUID id, String info) {
        logger.log(Level.INFO, info);
    }
}
