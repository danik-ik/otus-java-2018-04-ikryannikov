package ru.otus.danik_ik.homework16.dbserviceapp;

import ru.otus.danik_ik.homework16.dbserviceapp.cachedstorage.DbServiceCached;
import ru.otus.danik_ik.homework16.dbserviceapp.hibernateStorage.DbServiceHibernate;
import ru.otus.danik_ik.homework16.messageserver.app.ExchangeMessage;
import ru.otus.danik_ik.homework16.messageserver.app.MessageSystemContext;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;
import ru.otus.danik_ik.homework16.messageserver.app.addressees.DBAddressee;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgException;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgRegisterAddressee;
import ru.otus.danik_ik.homework16.messageserver.cache.CacheHelper;
import ru.otus.danik_ik.homework16.messageserver.db.storage.DBService;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.app.annotations.Blocks;
import ru.otus.danik_ik.homework16.messageserver.server.channel.IncomingMessagesProcessor;
import ru.otus.danik_ik.homework16.messageserver.server.channel.SocketMsgWorker;

import java.io.IOException;
import java.net.Socket;

public class DbMain
{
    private static String[] args;
    public static void main( String[] args ) throws IOException {
        DbMain.args = args;
        new DbMain().start();
    }

    // разобранные параметры командной строки
    private String host;
    private int port;
    private String addressId;

    private MessageSystemContext context;
    private MsgWorker worker;
    private DBAddressee db;
    DBService dbService;

    @Blocks
    private void start() throws IOException {
        parseParams();

        SocketMsgWorker worker = new SocketMsgWorker(new Socket(host, port));
        this.worker = worker;

        context = new MessageSystemContext(worker);

        DBService dbService = new DbServiceHibernate();
        DbServiceCached dbCached = new DbServiceCached(dbService, CacheHelper.getSoftCache(100));
        this.dbService = dbCached;
        db = new DbServiceImpl(addressId, context, this.dbService);

        createData();

        worker.init();
        worker.send(new MsgRegisterAddressee(db.getAddress()));

        ExchangeMessage exchangeMessage = IncomingMessagesProcessor.getExchangeMessage(worker);
        while (exchangeMessage != null) {
            try {
                exchangeMessage.exec(db);
            } catch (Exception e) {
                worker.send(new MsgException(db.getAddress(), exchangeMessage, e.getMessage()));
            }
            exchangeMessage = IncomingMessagesProcessor.getExchangeMessage(worker);
        }
    }

    private void createData() {
        final int MAX_INDEX = 10;
        for (int i = 0; i <= MAX_INDEX; i++) {
            UserDataSet user = new UserDataSet();
            user.setName("User " + i + " from " + addressId);
            user.setStreet("Noname street, " + i);
            dbService.save(user);
        }
    }

    private void parseParams() {
        try {
            host = args[0];
            port = Integer.valueOf(args[1]);
            addressId = args[2];
        } catch (Exception e) {
            System.out.println("command line parameters:\n" +
                    "1. Message system host\n" +
                    "2. Message system port\n" +
                    "3. Address ID for message system\n");
        }
    }
}
