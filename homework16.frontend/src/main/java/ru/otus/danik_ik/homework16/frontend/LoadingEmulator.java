package ru.otus.danik_ik.homework16.frontend;

import ru.otus.danik_ik.homework16.messageserver.db.storage.DBService;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendService;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendSocketMessageHandler;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiConsumer;

public class LoadingEmulator implements FrontendSocketMessageHandler {
    private static final int MAX_INDEX = 4;
    private static final int PAUSE = 300;

    private final FrontendService frontendService;

    public LoadingEmulator(FrontendService frontendService) {
        this.frontendService = frontendService;
        runAsThread();
    }

    public void runAsThread() {
        new Thread(this::run).start();
    }

    private void run() {
        createData();
        while (!Thread.currentThread().isInterrupted()) {
            frontendService.getUserById(getRandomIndex(), this);
            sleep(PAUSE);
        }
    }

    private long getRandomIndex() {
        return ThreadLocalRandom.current().nextLong(0, MAX_INDEX + 1);
    }

    private void sleep(int pause) {
        try {
            Thread.sleep(pause);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void createData() {
        for (int i = 0; i <= MAX_INDEX; i++) {
            UserDataSet user = new UserDataSet();
            user.setName("User " + i);
            user.setStreet("Noname street, " + i);
            frontendService.addUser(user, this);
        }
    }

    @Override
    public BiConsumer<UUID, UserDataSet> getResponseHandler() {
        return (a,b)->{};
    }

    @Override
    public BiConsumer<UUID, String> getExceptionHandler() {
        return (a,b)->{};
    }

    @Override
    public BiConsumer<UUID, String> getInfoHandler() {
        return (a,b)->{};
    }
}
