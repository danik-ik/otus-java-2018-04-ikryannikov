package ru.otus.danik_ik.homework16.frontend.dbfacade;

import ru.otus.danik_ik.homework16.messageserver.app.ExchangeMessage;
import ru.otus.danik_ik.homework16.messageserver.app.MessageSystemContext;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgAddUser;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgGetUserById;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgUpdateUser;
import ru.otus.danik_ik.homework16.messageserver.cache.CacheEngine;
import ru.otus.danik_ik.homework16.messageserver.cache.CacheEngineImpl;
import ru.otus.danik_ik.homework16.messageserver.cache.CacheHelper;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendService;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendSocketMessageHandler;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.MessageSystem;
import ru.otus.danik_ik.homework16.messageserver.server.channel.IncomingMessagesProcessor;

import java.util.UUID;

public class FrontendServiceImpl implements FrontendService, AutoCloseable {
    private static int RESPONSE_TIMEOUT_MILLIS = 180 * 1000;

    private final MessageSystemContext context;
    private final MsgWorker worker;
    private final FrontendSocketMessageHandler defaultHandler;

    CacheEngine<UUID, FrontendSocketMessageHandler> requestOwners =
            new CacheEngineImpl<>(-1, RESPONSE_TIMEOUT_MILLIS, 0, false,
            CacheHelper.SoftEntryFactory());

    private IncomingMessagesProcessor processor;

    public FrontendServiceImpl(MessageSystemContext context, MsgWorker worker, FrontendSocketMessageHandler defaultHandler) {
        this.context = context;
        this.worker = worker;
        processor = new IncomingMessagesProcessor(this, worker);
        this.defaultHandler = defaultHandler;
    }

    @Override
    public void init() {
        processor.start();
    }

    @Override
    public void completeRequest(UUID id, String response) {
        handleInfo(id, response);
    }

    @Override
    public void completeRequest(UUID id, UserDataSet userDataSet) {
        FrontendSocketMessageHandler requestOwner = getRequestOwners(id);
        if (requestOwner == null) requestOwner = defaultHandler;
        requestOwner.getResponseHandler().accept(id, userDataSet);
    }

    @Override
    public void update(UserDataSet dataSet, FrontendSocketMessageHandler socket) {
        ExchangeMessage msg = new MsgUpdateUser(getAddress(), context.getDbAddress(), dataSet);
        context.getMessageSystem().send(msg);
        registerRequestOwner(msg.getRequestId(), socket);
    }

    @Override
    public void getUserById(long id, FrontendSocketMessageHandler socket) {
        ExchangeMessage msg = new MsgGetUserById(getAddress(), context.getDbAddress(), id);
        context.getMessageSystem().send(msg);
        registerRequestOwner(msg.getRequestId(), socket);
    }

    @Override
    public void addUser(UserDataSet dataSet, FrontendSocketMessageHandler socket) {
        ExchangeMessage msg = new MsgAddUser(getAddress(), context.getDbAddress(), dataSet);
        context.getMessageSystem().send(msg);
        registerRequestOwner(msg.getRequestId(), socket);
    }

    public void registerRequestOwner(UUID requestId, FrontendSocketMessageHandler socket) {
        requestOwners.put(requestId, socket);
    }

    @Override
    public void handleException(UUID id, String exceptionInfo) {
        FrontendSocketMessageHandler requestOwner = getRequestOwners(id);
        if (requestOwner == null) requestOwner = defaultHandler;
        requestOwner.getExceptionHandler().accept(id, exceptionInfo);
    }

    @Override
    public void handleInfo(UUID id, String info) {
        FrontendSocketMessageHandler requestOwner = getRequestOwners(id);
        if (requestOwner == null) requestOwner = defaultHandler;
        requestOwner.getInfoHandler().accept(id, info);
    }

    private FrontendSocketMessageHandler getRequestOwners(UUID id) {
        FrontendSocketMessageHandler result = requestOwners.get(id);
        if (result == null) {
            defaultHandler.getExceptionHandler().accept(id, "unknown request owner or response timeout expired");
        }
        return result;
    }

    @Override
    public Address getAddress() {
        return context.getFrontAddress();
    }

    @Override
    public MessageSystem getMS() {
        return worker;
    }

    @Override
    public void close() throws Exception {
        processor.close();
    }
}
