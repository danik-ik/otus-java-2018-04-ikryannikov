package ru.otus.danik_ik.homework16.frontend.user;

import com.google.gson.Gson;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import ru.otus.danik_ik.homework16.messageserver.app.serialization.GsonHelper;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendService;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendSocketMessageHandler;

import java.io.IOException;
import java.util.UUID;
import java.util.function.BiConsumer;

@WebSocket
public class UserWebSocket implements FrontendSocketMessageHandler {
    private Session session;

    private FrontendService frontendService;

    public UserWebSocket(FrontendService frontendService) {
        this.frontendService = frontendService;
    }

    @OnWebSocketMessage
    public void onMessage(String data) {
        sendToClient(sendToDb(data));
    }

    private String sendToDb(String data) {
        int delimiterPosition = data.indexOf(':');

        String command = delimiterPosition >= 0 ?
                data.substring(0, delimiterPosition) : data;
        String tail = delimiterPosition >= 0 ?
                data.substring(delimiterPosition + 1) : "";

        switch (command) {
            case "ADD": {
                addUser(tail);
                break;
            }
            case "UPDATE": {
                updateUser(tail);
                break;
            }
            case "READ": {
                readUser(tail);
                break;
            }
            default: {return "REJECTED";}
        }
        return "ACCEPTED";
    }

    private void readUser(String jsonStr) {
        UserID userID = new Gson().fromJson(jsonStr, UserID.class);
        try {
            frontendService.getUserById(userID.id, this);
        } catch (Exception e) {
            thereWasError(e);
            return;
        }
    }

    @Override
    public BiConsumer<UUID, UserDataSet> getResponseHandler() {
        return this::sendUserDataSetToClient;
    }

    @Override
    public BiConsumer<UUID, String> getExceptionHandler() {
        return this::handleErrorMessage;
    }

    @Override
    public BiConsumer<UUID, String> getInfoHandler() {
        return this::handleErrorMessage;
    }

    private class UserID {
        Long id;
    }

    private void updateUser(String jsonStr) {
        UserDataSet userDataSet = GsonHelper.userFromJson(jsonStr);
        try {
            frontendService.update(userDataSet, this);
        } catch (Exception e) {
            // В реальности ошибка при сохранении сюда не попадёт, т.к. текущая реализация не предусматривает
            // возврат информации об ошибке через систему сообщений
            thereWasError(e);
            return;
        }
    }

    private void thereWasError(Exception e) {
        sendToClient("ERROR");
    }

    private void handleErrorMessage(UUID requestId, String errorMessage) {
        sendToClient("ERROR:" + new Gson().toJson(errorMessage));
    }

    private void handleInfoMessage(UUID requestId, String infoMessage) {
        sendToClient("INFO:" + new Gson().toJson(infoMessage));
    }

    private void addUser(String jsonStr) {
        UserDataSet userDataSet = GsonHelper.userFromJson(jsonStr);
        try {
            frontendService.addUser(userDataSet, this);
        } catch (Exception e) {
            thereWasError(e);
            return;
        }
    }

    private void sendUserDataSetToClient(UUID requestId, UserDataSet userDataSet) {
        String response = null;
        try {
            response = "USER:" + GsonHelper.toJson(userDataSet);
        } catch (Exception e) {
            // usrDataset загружается из БД лениво, поэтому ошибка ловится здесь
            // (в момент обращения к полям)
            thereWasError(e);
            return;
        }
        sendToClient(response);
    }

    public void sendToClient(String data) {
        try {
            this.getSession().getRemote().sendString(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnWebSocketConnect
    public void onOpen(Session session) {
        setSession(session);
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        // TODO: 15.09.2018 лог
    }

    public void init() {
        // do nothing
    }
}
