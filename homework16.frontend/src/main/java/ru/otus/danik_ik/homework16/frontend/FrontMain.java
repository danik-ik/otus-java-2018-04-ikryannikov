package ru.otus.danik_ik.homework16.frontend;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ru.otus.danik_ik.homework16.frontend.dbfacade.FrontendServiceImpl;
import ru.otus.danik_ik.homework16.frontend.servlet.AdminServlet;
import ru.otus.danik_ik.homework16.frontend.servlet.LoginServlet;
import ru.otus.danik_ik.homework16.frontend.servlet.TemplateProcessor;
import ru.otus.danik_ik.homework16.frontend.user.UserServlet;
import ru.otus.danik_ik.homework16.messageserver.app.MessageSystemContext;
import ru.otus.danik_ik.homework16.messageserver.app.MsgWorker;
import ru.otus.danik_ik.homework16.messageserver.app.messages.MsgRegisterAddressee;
import ru.otus.danik_ik.homework16.messageserver.cache.CacheEngine;
import ru.otus.danik_ik.homework16.messageserver.cache.CacheHelper;
import ru.otus.danik_ik.homework16.messageserver.db.storage.dataSets.UserDataSet;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendService;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendSocketMessageHandler;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Address;
import ru.otus.danik_ik.homework16.messageserver.messageSystem.Message;
import ru.otus.danik_ik.homework16.messageserver.server.channel.SocketMsgWorker;

import java.net.Socket;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.logging.Logger;

public class FrontMain
{
    private static Logger logger = Logger.getLogger(FrontMain.class.getName());
    private static String[] args;

    public static void main( String[] args ) throws Exception {
        FrontMain.args = args;
        new FrontMain().start();
    }

    private final static String PUBLIC_HTML = "public_html";
    // разобранные параметры командной строки
    private String msgHost;
    private int msgPort;
    private int webPort;
    private String frontAddressId;
    private String dbAddressId;

    private MsgWorker worker;

    private void start() throws Exception {
        parseParams();

        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setResourceBase(PUBLIC_HTML);

        CacheEngine<Long, UserDataSet> cacheEngine = CacheHelper.getSoftCache(100);

        ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);

        try (Socket socket = new Socket(msgHost, msgPort);
             SocketMsgWorker worker = new SocketMsgWorker(socket);) {
            MessageSystemContext msgContext = new MessageSystemContext(worker);
            msgContext.setFrontAddress(new Address(frontAddressId));
            msgContext.setDbAddress(new Address(dbAddressId));

            FrontendSocketMessageHandler defaultHandler = new DefaultHandler();
            try (FrontendService frontendService = new FrontendServiceImpl(msgContext, worker, defaultHandler)) {


                worker.init();
                Message msg = new MsgRegisterAddressee(frontendService.getAddress());

                frontendService.init();
                ((FrontendServiceImpl) frontendService).registerRequestOwner(msg.getRequestId(), defaultHandler);

                worker.send(msg);

//                LoadingEmulator loadingEmulator = new LoadingEmulator(frontendService);

                servletContextHandler.addServlet(LoginServlet.class, "/login");
                servletContextHandler.addServlet(new ServletHolder(new AdminServlet(cacheEngine, new TemplateProcessor())), "/admin");

                UserServlet userServlet = new UserServlet(frontendService);
                servletContextHandler.addServlet(new ServletHolder(userServlet), "/wsUser");

                Server server = new Server(webPort); // Jetty server
                server.setHandler(new HandlerList(resourceHandler, servletContextHandler));

                server.start();
                server.join();
            }
        }
    }

    private void parseParams() {
        // TODO: 14.09.2018 добавить реальный разбор
        webPort = 8080;
        msgHost = "localhost";
        msgPort = 5050;
        frontAddressId = "frontend";
        dbAddressId = "DB";
        try {
            webPort = Integer.valueOf(args[0]);
            msgHost = args[1];
            msgPort = Integer.valueOf(args[2]);
            frontAddressId = args[3];
            dbAddressId = args[4];
        } catch (Exception e) {
            System.out.println("command line parameters:\n" +
                    "1. web port\n" +
                    "2. Message system msgHost\n" +
                    "3. Message system port\n" +
                    "4. Address ID for message system\n" +
                    "5. Database address ID\n"
            );
        }
    }

    private class DefaultHandler implements FrontendSocketMessageHandler {

        @Override
        public BiConsumer<UUID, UserDataSet> getResponseHandler() {
            return (uuid, user) -> logger.warning("Message id=" + uuid + ": what am I doing there?!!");
        }

        @Override
        public BiConsumer<UUID, String> getExceptionHandler() {
            return (uuid, msg) -> logger.warning("Message id=" + uuid + ": " + msg);
        }

        @Override
        public BiConsumer<UUID, String> getInfoHandler() {
            return (uuid, msg) -> logger.info("Message id=" + uuid + ": " + msg);
        }
    }
}
