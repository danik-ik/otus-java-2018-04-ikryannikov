package ru.otus.danik_ik.homework16.frontend.user;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendService;

import java.util.concurrent.TimeUnit;

public class UserServlet extends WebSocketServlet {
    private final static long LOGOUT_TIME = TimeUnit.MINUTES.toMillis(10);
    private final FrontendService frontendService;

    public UserServlet(FrontendService frontendService) {
        this.frontendService = frontendService;
    }


    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.getPolicy().setIdleTimeout(LOGOUT_TIME);
        factory.setCreator(new UserWebSocketCreator(frontendService));
    }
}
