package ru.otus.danik_ik.homework16.frontend.user;

import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import ru.otus.danik_ik.homework16.messageserver.frontend.FrontendService;

public class UserWebSocketCreator  implements WebSocketCreator {
    private final FrontendService frontendService;

    public UserWebSocketCreator(FrontendService frontendService) {
        this.frontendService = frontendService;
    }

    @Override
    public Object createWebSocket(ServletUpgradeRequest servletUpgradeRequest, ServletUpgradeResponse servletUpgradeResponse) {
        UserWebSocket webSocket = new UserWebSocket(frontendService);
        webSocket.init();
        return webSocket;
    }
}
